package urogapBot2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.json.JSONArray;
import org.json.JSONObject;

public class UrogapBot implements Runnable {
	
	static String server;
	static String nick;
	static String password;
	static String channel;
	
	long streamStart = new GregorianCalendar(2016, 2, 18, 22, 4).getTimeInMillis();
	
	ScriptEngine scriptengine;

	Socket socket;
	BufferedReader input;
	BufferedWriter output;
	
	ArrayList<String> usersOnline = new ArrayList<>();
	ArrayList<String> moderators = new ArrayList<>();
	
	HashMap<String, Command> commands = new HashMap<>();
	
	boolean muted = false;
	
	int xdcount = 0;
	
	@Override
	public void run() {
		
		try {
			
			init();
			
			//Script engine for Javscript code execution.
			scriptengine = new ScriptEngineManager().getEngineByName("js");
		
			//Open socket to server.
			socket = new Socket(server, 6667);
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			
			//Login to server. If login fails throw exception.
			if(!IRClogin(nick, password))
				throw new IllegalStateException("Could not login to " + server);
			
			//Join channel
			sendString("JOIN " + channel);
			//request membership capabilities
			sendString("CAP REQ :twitch.tv/membership");
			
			//get current online users list
			getUsersListOnStart();
			
			//Loop until socket is closed.
			String line = "";
			while((line = input.readLine()) != null) {
				parseLine(line);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(socket != null) {
				try {
					socket.close();
				} catch (IOException e) { e.printStackTrace(); }
			}
		}
	}
	
	/**
	 * Initialize stuff.
	 * @throws IOException
	 */
	private void init() throws IOException {
		
		//Read the xd count from file.
		File xdfile = new File("xd.txt");
		if(xdfile.exists()) {
			BufferedReader xdreader = new BufferedReader(new FileReader(xdfile));
			xdcount = Integer.parseInt(xdreader.readLine());
			xdreader.close();
		}
		
		//Add all commands to the commands map.
		commands.put("!help", new Command("Command info.", false) {
			@Override
			public void command(String username, String[] args) throws IOException {
				Set<String> keys = commands.keySet();
				boolean isMod = moderators.contains(username);
				String help = "";
				for(String cmd : keys) {
					String desc = commands.get(cmd).getDescription();
					if(commands.get(cmd).isModsOnly() && !isMod)
						continue;
					help += cmd + ": " + desc + " - ";
				}
				sendMessage(channel, help.substring(0, help.length() - 3));
			}
		});
		commands.put("!faq", new Command("Preguntas frecuentes.", false) {
			@Override
			public void command(String username, String[] args) throws IOException {
				sendMessage(channel, "1. VaultBoy Estoy haciendo un mmorts para navegador web gratuito, nada de pay to wins. 2. VaultBoy Esta programado en php y javascript. 3. VaultBoy No, no puedo ense�ar la pantalla. 4. VaultBoy Las 140 horas son en 10 dias, no son seguidas. 5. VaultBoy Hablo en algunos momentos, pero para concentrarme no lo hago, leo el chat... *. VaultBoy �Alguna pregunta que a�adir?");
			}
		});
		commands.put("!xdpizza", new Command("Contar xd's", true) {
			@Override
			public void command(String username, String[] args) throws IOException {
				sendMessage(channel, "pizzalated_ ha dicho xd " + xdcount + (xdcount == 1 ? " vez." : " veces."));
			}
		});
		commands.put("!uptime", new Command("Tiempo online.", false) {
			@Override
			public void command(String username, String[] args) throws IOException {
				sendMessage(channel, getUptime() + " Kappa");
			}
		});
		commands.put("!urogap", new Command("Mutear/Desmutear a bot_urogap.", true) {
			@Override
			public void command(String username, String[] args) throws IOException {
				muted = !muted;
				sendMessage(channel, "/me " + (muted ? "muted." : "unmuted."), true);
			}
			@Override
			public void run(String username, String[] args, boolean userIsMod) throws IOException {
				if(isModsOnly() && !userIsMod) return;
				command(username, args);
			}
		});
		commands.put("!online", new Command("Usuarios online.", true) {
			@Override
			public void command(String username, String[] args) throws IOException {
				printOnlineUsers();
			}
		});
		commands.put("!mods", new Command("Moderadores online.", false) {
			@Override
			public void command(String username, String[] args) throws IOException {
				printModerators();
			}
		});
		commands.put("!song", new Command("Nombre de la cancion.", false) {
			@Override
			public void command(String username, String[] args) throws IOException {
				sendMessage(channel, "Darude - Sandstorm https://youtu.be/dQw4w9WgXcQ Kappa Kappa Kappa");
			}
		});
		commands.put("!solve", new Command("Resolver operaciones matematicas.", true) {
			@Override
			public void command(String username, String[] args) throws IOException {
				if(moderators.contains(username)) {
					String script = "";
					for(String s : args) script += s + " ";
					script = script.trim();
					if (!script.contains("while") && !script.contains("for") && !script.contains("function")
							&& !script.contains("eval")) {
						try {
							Object o = scriptengine.eval(script);
							if (o instanceof Number)
								sendMessage(channel, String.valueOf((Number) o));
							else if(o instanceof Boolean)
								sendMessage(channel, String.valueOf((boolean) o));
							else if(o instanceof String) {
								String result = ((String) o).trim();
								if(result.startsWith(".") || result.startsWith("!") || result.startsWith("/"))
									sendMessage(channel, "Comandos con el !solve? Ni de broma...");
								else if(checkForURLs(result))
									sendMessage(channel, "Intentando traficar con links?");
								else {
									sendMessage(channel, result);
								}
							}
						} catch (ScriptException e) { System.err.println("! Script error."); }
					}
				}
			}
		});
		commands.put("!link", new Command("Poner un link sin ban.", false) {
			@Override
			public void command(String username, String[] args) throws IOException { /* Handled in messageLine(). */ }
		});
	}
	
	/**
	 * Reads the line given and does whatever it needs to do for each type of line.
	 * @param line
	 * @throws IOException
	 */
	private void parseLine(String line) throws IOException {
		if(line.contains("PRIVMSG"))
			messageLine(line);
		else if(line.contains("MODE"))
			moderatorEvent(line);
		else if(line.contains("PING"))
			respondToPing(line);
		else if(line.contains("JOIN"))
			userJoined(line);
		else if(line.contains("PART"))
			userLeft(line);
		else
			System.out.println("> " + line);
	}
	
	/**
	 * This is called if the line is a PRIVMSG (message) line.
	 * @param line
	 * @throws IOException
	 */
	private void messageLine(String line) throws IOException {
		
		//Get message and username from the raw line.
		String message = getMessage(line);
		String username = getUserFromMessageLine(line);
		//Print the message.
		System.out.println("> " + username + ": " + message);
		
		//Stuff that happens to non-mods
		if(!moderators.contains(username)) {
			//Check if there are URLs in the message. If there are, timeout.
			if(checkForURLs(message) && !message.contains("!link ")) {
				sendMessage(channel, "/timeout " + username + " 60", true);
				sendMessage(channel, username + " -> Utiliza !link para poner un link.");
			}
			//Uppercase characters timeout.
			if(message.length() > 5) {
				int upperCount = 0;
				for(int i = 0; i < message.length(); i++) {
					if(Character.isUpperCase(message.charAt(i)) && Character.isAlphabetic(message.charAt(i)))
						upperCount++;
				}
				if(upperCount / (float) message.length() > 0.5) {
					sendMessage(channel, "/timeout " + username + " 60", true);
					sendMessage(channel, username + " -> NO PONGAS TANTAS MAYUS!");
				}
			}
		}
		
		//If it starts with ! then it's a command.
		if (message.startsWith("!"))
			parseUserCommand(message, username);
		
		//Pizza's xd counter.
		if(username.equalsIgnoreCase("pizzalated_") && message.contains("xd")) {
			String[] parts = message.split(" ");
			for(String s : parts)
				if(s.equals("xd")) xdcount++;
			File xdfile = new File("xd.txt");
			if(xdfile.exists()) xdfile.delete();
			xdfile.createNewFile();
			FileWriter fileWriter = new FileWriter(xdfile);
			fileWriter.write(String.valueOf(xdcount));
			fileWriter.close();
		}
		
	}
	
	/**
	 * Called if the line is a message and starts with '!'
	 * @param message
	 * @param username
	 * @throws IOException
	 */
	private void parseUserCommand(String message, String username) throws IOException {
		//Get the command.
		String[] parts = message.trim().split(" ");
		String command = parts[0].toLowerCase();
		
		//If the command doesn't exist, skip.
		if(!commands.containsKey(command)) return;
		
		//If muted, skip this unless it's !urogap command (which mutes/unmutes the bot)
		if(muted && !command.equalsIgnoreCase("!urogap"))
			return;
		
		//get the arguments of the command (excluding the command itself)
		String[] args = new String[parts.length - 1];
		for(int i = 0; i < parts.length - 1; i++)
			args[i] = parts[i + 1];
		
		//Execute the command.
		commands.get(command).run(username, args, moderators.contains(username));
	}
	
	/**
	 * Returns the message from a raw PRIVMSG line.
	 * @param line
	 * @return
	 */
	private String getMessage(String line) {
		String message = "";
		String[] parts = line.split(":");
		for(int i = 2; i < parts.length; i++)
			message += parts[i] + ":";
		message = message.substring(0, message.length() - 1);
		return message;
	}
	
	/**
	 * Returns the username from a raw PRIVMSG line.
	 * @param line
	 * @return
	 */
	private String getUserFromMessageLine(String line) {
		String[] parts = line.split(":");
		String username = parts[1].split("!")[0];
		return username.toLowerCase();
	}
	
	/**
	 * Returns true if the message contains one or multiple URLs.
	 * @param message
	 * @return
	 * @throws IOException
	 */
	private boolean checkForURLs(String message) throws IOException {
		String[] parts = message.split(" ");
		for(String p : parts) {
			for(String tld : TLD.TLDs) {
				Pattern pattern = Pattern.compile("\\w+(\\" + tld.toLowerCase() + ")\\b(\\/\\w+)*");
				Matcher matcher = pattern.matcher(p.toLowerCase());
				if(matcher.find())
					return true;
			}
		}
		return false;
	}
	
	/**
	 * Prints all users online (from onlineUsers list).
	 * @throws IOException
	 */
	private void printOnlineUsers() throws IOException {
		Iterator<String> it = usersOnline.iterator();
		String usersList = "";
		while(it.hasNext()) {
			usersList += it.next();
			if(it.hasNext()) usersList += " - ";
		}
		sendMessage(channel, "Users online (" + usersOnline.size() + "): " + usersList + ".");
	}
	
	/**
	 * Prints all moderators online (from moderators list).
	 * @throws IOException
	 */
	private void printModerators() throws IOException {
		Iterator<String> it = moderators.iterator();
		String modsList = "";
		while(it.hasNext()) {
			modsList += it.next();
			if(it.hasNext()) modsList += " - ";
		}
		sendMessage(channel, "Moderators: " + modsList + ".");
	}
	
	/**
	 * Executed every time a user joins the chat.
	 * @param line
	 */
	private void userJoined(String line) {
		String username = getUserFromMessageLine(line).toLowerCase();
		if(!usersOnline.contains(username)) {
			usersOnline.add(username);
			System.out.println("+ " + username + " joined.");
		}
	}
	
	/**
	 * Executed every time a user leaves the chat.
	 * @param line
	 */
	private void userLeft(String line) {
		String username = getUserFromMessageLine(line).toLowerCase();
		if(usersOnline.contains(username)) {
			usersOnline.remove(username);
			System.out.println("- " + username + " left.");
		}
	}
	
	/**
	 * Executes every time a user is given or revoked moderator rights.
	 * @param line
	 */
	private void moderatorEvent(String line) {
		String[] parts = line.split(" ");
		String username = parts[parts.length -1];
		if(line.contains("+o")) {
			if(!moderators.contains(username)) {
				moderators.add(username);
				System.out.println("! Gave moderator rights to " + username);
			}
		} else if(line.contains("-o")) {
			if(moderators.contains(username)) {
				moderators.remove(username);
				System.out.println("! Revoked moderator rights from " + username);
			}
		}
	}
	
	/**
	 * Responds to PING command with PONG to avoid disconnecting from the server.
	 * @param line
	 * @throws IOException
	 */
	private void respondToPing(String line) throws IOException {
		System.out.println("> " + line);
		sendString("PONG " + line.substring(5));
	}
	
	/**
	 * Calculates and returns the time pagoru has been on stream.
	 * @return
	 */
	private String getUptime() {
		long diff = System.currentTimeMillis() - streamStart;
		long seconds = diff / 1000;
		long minutes = seconds / 60;
		long hours = minutes / 60;
		long days = hours / 24;
		return days + " dias, " + hours % 24 + " horas, " + minutes % 60 + " minutos, " + seconds % 60 + " segundos";
	}
	
	/**
	 * Sends a PRIVMSG to the channel specified that contains message.
	 * @param channel
	 * @param message
	 * @throws IOException
	 */
	private void sendMessage(String channel, String message) throws IOException {
		sendMessage(channel, message, false);
	}
	
	/**
	 * Same as sendMessage(channel, message) but if overrideMute is true the message is sent even if bot is muted.
	 * @param channel
	 * @param message
	 * @param overrideMute
	 * @throws IOException
	 */
	private void sendMessage(String channel, String message, boolean overrideMute) throws IOException {
		if(muted && !overrideMute) return;
		sendString("PRIVMSG " + channel + " :" + message);
	}
	
	/**
	 * Sends a string to the server.
	 * @param string
	 * @throws IOException
	 */
	private void sendString(String string) throws IOException {
		System.out.println("< " + string);
		output.write(string + "\r\n");
		output.flush();
	}
	
	/**
	 * Requests the list of users online to the twitch api.
	 */
	private void getUsersListOnStart() {
		
		try {
			URL url = new URL("https://tmi.twitch.tv/group/user/" + channel.substring(1) + "/chatters");
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			String json = "";
			String line = "";
			while((line = reader.readLine()) != null) {
				json += line + "\n";
			}
			JSONObject obj =  new JSONObject(json);
			JSONArray mods = obj.getJSONObject("chatters").getJSONArray("moderators");
			JSONArray viewers = obj.getJSONObject("chatters").getJSONArray("viewers");
			for(int i = 0; i < mods.length(); i++) {
				String name = mods.getString(i).toLowerCase();
				if(!moderators.contains(name))
					moderators.add(name);
				if(!usersOnline.contains(name))
					usersOnline.add(name);
			}
			for(int i = 0; i < viewers.length(); i++) {
				String name = viewers.getString(i).toLowerCase();
				if(!usersOnline.contains(name))
					usersOnline.add(name);
			}
		} catch(IOException e) {
			System.err.println("Could not fetch users list from twitch api.");
		}
	}
	
	/**
	 * Sends the IRC server the password and nick to use. Returns true if login was successful, false otherwise.
	 * @param nick
	 * @param password
	 * @return
	 * @throws IOException
	 */
	private boolean IRClogin(String nick, String password) throws IOException {
		output.write("PASS " + password + "\r\n");
		output.write("NICK " + nick + "\r\n");
		output.flush();
		String line = "";
		while((line = input.readLine()) != null) {
			System.out.println("> " + line);
			if(line.indexOf("004") >= 0)
				return true;
		}
		return false;
	}
	
	/**
	 * Main.
	 * @param args
	 */
	public static void main(String[] args) {
		server = args[0];
		nick = args[1];
		channel = args[2];
		password = args[3];
		Thread thread = new Thread(new UrogapBot());
		thread.setName("Bot Urogap");
		thread.start();
	}

}
