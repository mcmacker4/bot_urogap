package urogapBot2;

import java.io.IOException;

public abstract class Command {
	
	static final long COMMAND_TIMEOUT = 15000;

	private long lastTimeExecuted;
	private String description;
	private boolean modsOnly;
	
	public Command(String description, boolean modsOnly) {
		this.description = description;
		this.modsOnly = modsOnly;
	}
	
	public void run(String username, String[] args, boolean userIsMod) throws IOException {
		if(modsOnly && !userIsMod) return;
		if(userIsMod)
			command(username, args);
		else if(lastTimeExecuted + COMMAND_TIMEOUT < System.currentTimeMillis()) {
			command(username, args);
			lastTimeExecuted = System.currentTimeMillis();
		}
	}
	
	public abstract void command(String username, String[] args) throws IOException;
	
	public String getDescription() {
		return description;
	}
	
	public boolean isModsOnly() {
		return modsOnly;
	}
	
}
